/*inspired from this course's exercises*/
const jwtDecode = require("jwt-decode");
const {config} = require("./Constants/Constants");

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function studentId() {
    const token = localStorage.getItem("portal-jwt-Token")
    const decoded = jwtDecode(token)
    return decoded.sub;
}

function redirect() {
    const url1 = config.url.API_URL+"/login"
    const token = getParameterByName("token");
    if (token != null && token.length > 0) {
        //Store token and redirect to baseURL
        localStorage.setItem("portal-jwt-Token", token);

        window.history.replaceState(null, "Diplomportal", "/")

    }
    if(!localStorage.getItem("portal-jwt-Token")){
        window.location.replace(url1)
        return false
    }
}

function logout (){
    localStorage.clear();
    if(!localStorage.getItem("portal-jwt-Token")){
        window.location.replace("/")
    }
}

function tokenExists(){
    return localStorage.getItem("portal-jwt-Token")
}

module.exports = {getParameterByName,redirect,logout,tokenExists,studentId}
