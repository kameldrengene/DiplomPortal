import {makeObservable, observable} from "mobx";
import {config} from "../Constants/Constants";
import fetchData from "./Utility";
class WeatherStore {

    api = config.url.API_URL+"/weather/"

    temperature = null;
    weatherCode = null;

    constructor() {
        makeObservable(this, {
            temperature: observable,
            weatherCode: observable
        })
    }

    async fetchWeatherData() {
        var token = " Bearer " + localStorage.getItem("portal-jwt-Token")
        await fetchData(()=>fetch(
            config.url.API_URL+"/weather/",
            {
                method: "GET",
                headers: {"Authorization": token}
            }
        ),(json)=>{
            try {
                this.temperature = json.air_temperature
                this.weatherCode = json.weather_code
            }catch (e) {
                console.log(e.toString())
            }
        })
    }



}

export const weatherStore = new WeatherStore()