import {makeObservable, observable, runInAction} from "mobx";
import {config} from "../Constants/Constants";
import fetchData from "./Utility";
class SingleCourseStore {

    courseName = "";
    courseInstructor = {}
    url1 = config.url.API_URL + "/courses/"
    url2 = "http://localhost:8080/courses/name/"

    constructor() {
        makeObservable(this, {
            courseName: observable,
            courseInstructor: observable
        })
    }

    async fetchCourses(id) {
        let token = 'Bearer ' + localStorage.getItem("portal-jwt-Token")
        await fetchData(()=>fetch(
            this.url1 + "name/" + id,
            {
                method: 'GET',
                headers: {
                    'Authorization': token
                }
            },
        ),(json)=> {
           runInAction(() => {
               this.courseName = json.string
           })
        }
        )
    }

    async fetchInstructor(id) {
        let token = 'Bearer ' + localStorage.getItem("portal-jwt-Token")

        const myHeaders = new Headers();
        myHeaders.append("Authorization", token);

        const requestOptions = {
            method: 'GET',
            headers: myHeaders,
            redirect: 'follow'
        };
        await fetchData(()=>fetch(this.url1 + id + "/instructor", requestOptions),(result)=>{
            console.log(result)
            runInAction(() => this.courseInstructor = result)
        })
    }
}

export const singleCourseStore = new SingleCourseStore()

