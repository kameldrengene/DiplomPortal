import {makeObservable, observable} from "mobx";
import {config} from "../Constants/Constants";

class LoginDemoStore {

    token = "";
    url1 = config.url.API_URL + "/authenticate/"

    constructor() {
        makeObservable(this, {
            token: observable,
        })
    }

    async fetchLogin() {
        const user = {
            username: "s20000",
            password: "dtu"
        }
        try {
            const response = await fetch(
                config.url.API_URL + "/authenticate",
                {
                    method: 'POST',
                    headers: {'Content-Type': 'application/json'},
                    body: JSON.stringify(user)
                },
            )
            this.token = await response.json()
            console.log(this.token)
        } catch (e) {
            console.log('error', e)
        }
    }
}
export const loginDemoStore = new LoginDemoStore()

