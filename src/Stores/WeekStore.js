import {makeObservable, observable} from "mobx";
import {config} from "../Constants/Constants";
import fetchData from "./Utility"

class WeekStore {

    weeks = [];
    url1 = config.url.API_URL+"/courses/"
    url2 = "http://localhost:8080/weeks/"
    constructor() {
        makeObservable(this, {
            weeks: observable,
        })
    }

    async fetchCourses(id,year) {
        var token = 'Bearer ' + localStorage.getItem("portal-jwt-Token")
        await fetchData(()=>fetch(
            this.url1 + id + "/year/" + year,
            {method: 'GET',
                headers: {'Authorization': token}
            },
        ),(json)=>{
            this.weeks = json
            }
        )
    }
}


export const weekStore = new WeekStore()

