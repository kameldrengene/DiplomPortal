import {makeObservable, observable, runInAction} from "mobx";
import axios from "axios";
import {config} from "../Constants/Constants";
import fetchData from "./Utility";

const sendCourseObj = (grade,year,studentsid,courseid) => {
  let obj = {
      grade : grade,
      year : year,
      students_id : studentsid,
      courses_id : courseid
  }
  return (obj)
}
class StudentCourseStore {

    semesterdata = [];
    coursedata = []
    map = null
    ectCounts = 0

    url1 = config.url.API_URL+"/students/grades"
    url2 = "http://localhost:8080/students/grades"
    url3 = "http://localhost:8080/students/student"
    url4 = "http://localhost:8080/students/update/takes"
    url5 = config.url.API_URL+"/students/update/takes"

    constructor() {
        makeObservable(this, {
            semesterdata: observable,
            coursedata:observable,
            map:observable,
            ectCounts:observable
        })
    }

    fetchMultipleData () {
        const semesterCourses = config.url.API_URL+"/students/studyPlaner/courses/"
        const allCourses = config.url.API_URL+"/courses/"
        var token = 'Bearer ' + localStorage.getItem("portal-jwt-Token")
        try {
            const getsemesterCourses = axios.get(semesterCourses, {method: 'GET', headers: {'Authorization': token}})
            const getallCourses = axios.get(allCourses, {method: 'GET', headers: {'Authorization': token}})
            axios.all([getsemesterCourses, getallCourses]).then(
                axios.spread((...allData) => {
                    const result = allData[0].data
                    let allcourses = allData[1].data
                    console.log(result[0].year)
                    let years = []
                    let studentSemesterCourses = []
                    let courseIds = []
                    let countect = 0
                    for (const course of result) {
                        if (!years.includes(course.year)) {
                            years.push(course.year)
                        }
                        courseIds.push(course.courses_id)
                        countect = (countect + course.ects);
                    }
                    this.ectCounts = countect
                    for (const course of courseIds) {
                        for (let j = 0; j < allcourses.length; j++) {
                            if (allcourses[j].id === course) {
                                allcourses.splice(j, 1)
                            }
                        }
                    }
                    let i = 1;
                    const map = new Map()
                    Array.apply(null, Array(6)).map(function (x, index) {
                        let startYear = result[0].year + index;
                        let semester = {
                            nr: i,
                            year: startYear,
                            courses: []
                        }
                        for (const course of result) {
                            if (course.year === semester.year) {
                                semester.courses.push(course)
                            }
                        }
                        studentSemesterCourses.push(semester)
                        map.set(startYear, index)
                        i++;
                    })
                    runInAction(() => {
                        this.semesterdata = studentSemesterCourses
                        this.coursedata = allcourses
                        this.map = map
                    })
                })
            ).catch(function (error) {
                if (error.response) {
                    if (error.response.status === 403) {
                        localStorage.clear()
                        window.location.replace("/")
                    } else {
                        alert(" Server not responding correctly. Data might be missing. ")
                        console.log("status: " + error.response.status)
                    }
                }
            });
        }catch (e) {
            alert(" Server not responding. Data might be missing. ")
            console.log("error: "+e.toString())
        }
    }

    async updateStudiePlanLaegger(){
        let takeCourses = []
        this.semesterdata.map((course)=>{
            course.courses.map((in_course)=>{
                const objCourse = sendCourseObj(in_course.grade,in_course.year,in_course.students_id,in_course.courses_id)
                takeCourses.push(objCourse)
            })
        })
        console.log(takeCourses)
        var token = 'Bearer ' + localStorage.getItem("portal-jwt-Token")

        await fetchData(()=>fetch(this.url5,{
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'authorization': token
                // 'Content-Type': 'application/x-www-form-urlencoded',
            },
            body: JSON.stringify(takeCourses)
        }),(data)=>{
            if(data){
                alert("Successfully updated")
            }
        })
    }
}

export const studentCourseStore = new StudentCourseStore()