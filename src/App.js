import './App.css';
import NavigationBar from "./Components/NavigationBar/NavigationBar";
import 'bootstrap/dist/css/bootstrap.css';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import kurser from "./Components/Kurser";
import kalender from "./Components/Kalender";
import karakter from "./Components/Karakter";
import kursus from "./Components/Kursus";
import StudiePlanLaegger from "./Components/StudiePlanlægger/StudiePlanLaegger";
import * as serviceWorkerRegistration from './serviceWorkerRegistration';


import {redirect} from "./TokenHandler";
import {observer} from "mobx-react";


serviceWorkerRegistration.register();

window.addEventListener('beforeinstallprompt', (e) => {
    // Stash the event so it can be triggered later.
    console.log("Got beforeinstallprompt");
    window.deferredPrompt = e;
});

function Routing(){
    try {
        console.log("Courses loaded")
        return(<Router>
            <NavigationBar />
            <Switch>
                <Route exact path="/" component={kurser}/>
                <Route path="/kalender" component={kalender}/>
                <Route path="/karakter" component={karakter}/>
                <Route path="/course/:id/:year" component={kursus}/>
                <Route path="/studieplanlaegger" component={StudiePlanLaegger}/>
                <Route path="/" component={kurser}/>
            </Switch>
        </Router>)
    }catch (err){
        console.log("Error loading " + err)
    }
}

function App() {
    redirect()
  return (
    <div className="App">
        {Routing()}
    </div>
  );
}

export default observer(App);
