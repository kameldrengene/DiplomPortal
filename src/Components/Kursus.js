import React from "react";
import {Grid} from "@material-ui/core";
import {observer} from "mobx-react";
import {singleCourseStore} from "../Stores/SingleCourseStore";
import {weekStore} from "../Stores/WeekStore";
import {createData, BasicTable} from "./CourseTable";
import {Box, Container} from "@mui/material";
import {navigationStore} from "../Stores/NavigationStore";

var id
var year

function makeTable() {
    var tableList = [];
    weekStore.weeks.map((week) => (tableList.push(createData(week.description, week.homework, week.assignments, week.files))))
    return BasicTable(tableList)
}

class kursus extends React.Component {
    constuctor() {
        this.routeChange = this.routeChange.bind(this);
    }

    routeChange(newPath) {
        this.props.history.push(newPath);
    }

    componentDidMount() {
        singleCourseStore.fetchCourses(this.props.match.params.id)
        weekStore.fetchCourses(this.props.match.params.id, this.props.match.params.year)
        singleCourseStore.fetchInstructor(this.props.match.params.id)

    }

    render() {
        navigationStore.updatePage(0)
        id = this.props.match.params.id
        year = this.props.match.params.year
        return (
            <Box sx={{m:5}}>
                <Box
                    sx={{
                        display: 'flex',
                        justifyContent: 'space-between',
                        mt: 2,
                        mb: 2

                    }}
                >
                    <Box>
                        <h3 style={{textAlign: "left"}}>{id}</h3>
                        <h1 style={{textAlign: "left"}}>{singleCourseStore.courseName + " " + year}</h1>
                    </Box>
                    <Box>
                        <h3 style={{textAlign: "right"}}>{singleCourseStore.courseInstructor.firstName} {singleCourseStore.courseInstructor.lastName}</h3>
                        <h6 style={{textAlign: "right"}}>{singleCourseStore.courseInstructor.link}</h6>
                        <h6 style={{textAlign: "right"}}>{singleCourseStore.courseInstructor.mail}</h6>
                    </Box>
                </Box>
                <Grid container spacing={2} item xs={12} md={12}>
                    {makeTable()}
                </Grid>
            </Box>
        )
    }
}

export default observer(kursus);