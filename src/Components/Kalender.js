import React from "react";
import {Box, Typography} from "@mui/material";
import {Container} from "@material-ui/core";
import CourseStore from "../Stores/CourseStore";
import {observer} from "mobx-react";
import Semester from "./Semester";
import {runInAction} from "mobx";
import {navigationStore} from "../Stores/NavigationStore";

const kalenderstore = new CourseStore();

class Kalender extends React.Component {
    constuctor() {
        this.routeChange = this.routeChange.bind(this);
    }

    routeChange(newPath) {
        this.props.history.push(newPath);
    }
    componentDidMount() {
        runInAction(()=>kalenderstore.fetchWeeks())
    }

    render() {
        navigationStore.updatePage(1)
        return (
            <Box>
                <Semester store={kalenderstore}/>
                <Container>
                    <div className="weekgrid">
                        {kalenderstore.weeks.map((day) =>

                            <Box key={day.day} sx={{
                                height: "500px",
                                width: "100%",
                                backgroundColor: "lightgray",
                                borderRadius: 1,
                                border: 2,
                                borderColor: "darkgray"
                            }}>


                                <Box sx={{height: "45%"}}>
                                    <Box
                                        sx={{
                                            display: 'flex',
                                            justifyContent: 'center'
                                        }}
                                    >
                                        <Typography align="center" sx={{padding: "5px", color: "gray"}}>
                                            {day.day}
                                        </Typography>
                                    </Box>

                                    <Box
                                        sx={{
                                            display: 'flex',
                                            justifyContent: 'flex-start',
                                            position: "relative"
                                        }}
                                    >
                                        <Typography align="left" sx={{
                                            padding: "5px",
                                            color: "gray",
                                            position: "absolute",
                                            top: "-35px"
                                        }}>
                                            08:00
                                        </Typography>
                                    </Box>

                                    <Box
                                        sx={{display: 'grid', gridTemplateColumns: 'repeat(1, 1fr)', height: "85%"}}
                                        onClick={() => {
                                            this.routeChange("/course/" + day.coursemorning.courses_id + "/" + day.coursemorning.year)
                                        }}
                                    >
                                        <Box sx={{backgroundColor: day.morning.color}}>
                                            <Typography align="left" sx={{padding: "5px"}}>
                                                {day.morning.id}
                                            </Typography>
                                            <Typography>
                                                <strong>{day.morning.name}</strong>
                                            </Typography>
                                        </Box>
                                    </Box>

                                </Box>

                                <Box sx={{
                                    height: "10%",
                                    display: 'flex',
                                    alignItems: 'center',
                                    borderTop: 3,
                                    borderBottom: 2,
                                    borderColor: "darkgray"
                                }}>
                                    <Typography align="left" sx={{margin: "5px", color: "gray"}}>
                                        12:00
                                    </Typography>
                                </Box>

                                <Box sx={{height: "45%"}}>
                                    <Typography align="left" sx={{padding: "5px", color: "gray"}}>
                                        13:00
                                    </Typography>

                                    <Box
                                        sx={{display: 'grid', gridTemplateColumns: 'repeat(1, 1fr)', height: "85%"}}
                                        onClick={() => {
                                            this.routeChange("/course/" + day.courselunch.courses_id + "/" + day.courselunch.year)
                                        }}
                                    >
                                        <Box sx={{backgroundColor: day.lunch.color}}>
                                            <Typography align="left" sx={{padding: "5px"}}>
                                                {day.lunch.id}
                                            </Typography>
                                            <Typography>
                                                <strong>{day.lunch.name}</strong>
                                            </Typography>
                                        </Box>
                                    </Box>
                                </Box>
                            </Box>
                        )}

                    </div>
                </Container>
            </Box>
        )

    }
}

export default observer(Kalender);
