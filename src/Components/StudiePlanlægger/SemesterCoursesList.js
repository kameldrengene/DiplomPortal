import Box from "@mui/material/Box";
import {Typography} from "@mui/material";
import Paper from "@mui/material/Paper";
import {ThemeProvider} from "@emotion/react";
import CourseCard from "./SemesterCourseCard";
import {Draggable, Droppable} from "react-beautiful-dnd";
import React from "react";
import {createTheme} from "@mui/material/styles";
import {observer} from "mobx-react";

const colors = () => {
    return ({
        Red: "#990000",
        Darkgrey: "#C4C4C4",
        Mediumgrey: "#D9D9D9",
        Lightgrey: "#ECECEC"
    })
}
/*Component creates a single semester list*/
function SemesterCoursesList({courses, index}) {

    let courseTheme = createTheme();
    courseTheme.typography.h5 = {
        fontSize: '0.9rem',
        '@media (min-width:600px)': {
            fontSize: '1.0rem',
        },
        [courseTheme.breakpoints.up('md')]: {
            fontSize: '1.2rem',
        },
    }

    const thisYear = new Date().getUTCFullYear()
    if (courses.year <= thisYear) {
        console.log("im here")
        return (
            <Box
                sx={{
                display: 'flex',
                justifyContent: 'center'
            }}>
                <Box
                    data-testid = {"semesterBox-element"+index}
                    sx={{
                        display: 'flex',
                        justifyContent: 'flex-start',
                        position: "relative"
                    }}
                >
                    <Typography variant="h6" align="left" sx={{
                        position: "absolute",
                        top: "-35px"
                    }}>
                        <h4>{(index + 1)}.Semester</h4>
                    </Typography>
                </Box>
                <Paper
                    data-testid = {"semesterPaper-element"+index}
                    style={{
                        maxWidth: "400px",
                        minHeight: "100px",
                        minWidth: "400px",
                        borderRadius:"12px",
                        p: 2,
                        backgroundColor: colors().Lightgrey
                    }}>
                    <ThemeProvider theme={courseTheme}>
                        <Box
                            sx={{padding: "20px"}}
                        >
                            {
                                courses.courses.map((course, index) =>
                                    <CourseCard id={course.courses_id} name={course.name} index={index}
                                                ects={course.ects}
                                                day={course.day} time={course.time} isNotDraggable={true}/>
                                )
                            }
                        </Box>
                    </ThemeProvider>
                </Paper>
            </Box>
        )

    } else {
        return (
            <Box sx={{
                display: 'flex',
                justifyContent: 'center'
            }}>
                <Box
                    data-testid = {"semesterBox-element"+index}
                    sx={{
                        display: 'flex',
                        justifyContent: 'flex-start',
                        position: "relative"
                    }}
                >
                    <Typography variant="h6" align="left" sx={{
                        position: "absolute",
                        top: "-35px"
                    }}>
                        <h4>{(index + 1)}.Semester</h4>
                    </Typography>
                </Box>
                <Droppable droppableId={courses.year.toString()}>
                    {
                        (provided, snapshot) => (
                            <Paper
                                data-testid = {"semesterPaper-element"+index}
                                ref={provided.innerRef}
                                style={{
                                    maxWidth: "400px",
                                    minHeight: "100px",
                                    minWidth: "400px",
                                    borderRadius:"12px",
                                    p: 2,
                                    backgroundColor: colors().Lightgrey
                                }}>
                                <ThemeProvider theme={courseTheme}>
                                    <Box sx={{padding: "20px"}}>
                                        {
                                            courses.courses.map((course, index) => (
                                                    <Draggable
                                                        key={course.courses_id.toString()}
                                                        draggableId={course.courses_id.toString()}
                                                        index={index}>
                                                        {(provided) => (
                                                            <div
                                                                ref={provided.innerRef}
                                                                {...provided.draggableProps}
                                                                {...provided.dragHandleProps}
                                                            >
                                                                <CourseCard id={course.courses_id} name={course.name}
                                                                            index={index} day={course.day}
                                                                            time={course.time} ects={course.ects}
                                                                            isNotDraggable={false}
                                                                />
                                                            </div>
                                                        )}

                                                    </Draggable>
                                                )
                                            )
                                        }
                                    </Box>
                                </ThemeProvider>
                                {provided.placeholder}
                            </Paper>
                        )
                    }
                </Droppable>
            </Box>
        )
    }
}

export default observer(SemesterCoursesList);