import React, {useEffect} from "react";
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import {Button, Typography} from "@mui/material";
import {observer} from "mobx-react";
import {DragDropContext} from "react-beautiful-dnd";
import {studentCourseStore} from "../../Stores/StudentCourseStore";
import jwtDecode from "jwt-decode";
import {Container} from "@material-ui/core";
import KurserList from "./AllCoursesList";
import SemesterCoursesList from "./SemesterCoursesList";
import {navigationStore} from "../../Stores/NavigationStore";

const semesterCourseObject = (studentId, courseId, name, day, time, year, ect) => {
    let semester = {
        grade: -1,
        students_id: studentId,
        courses_id: courseId,
        name: name,
        year: year,
        day: day,
        time: time,
        ects: ect
    }
    return (semester)
}

const courseObject = (courseid, name, day, time, image, ects) => {
    let course = {
        id: courseid,
        name: name,
        day: day,
        time: time,
        image: image,
        ects: ects
    }
    return (course)
}
const studentId = () => {
    const token = localStorage.getItem("portal-jwt-Token")
    try {
        const decoded = jwtDecode(token)
        console.log(decoded)
        return parseInt(decoded.sub.substring(1));
    } catch (e) {
        alert(e)
    }
}


function StudiePlanLaegger(props) {

    navigationStore.updatePage(3)

    useEffect(() => {
        if(props.data){
            studentCourseStore.coursedata = props.data
        }else {
            studentCourseStore.fetchMultipleData()
        }
    }, [])


    const handleDragEnd = (data) => {
        if (!data.destination) {
            return
        }
        const courseId = data.draggableId
        const source = data.source
        const destination = data.destination
        const sourseYear = source.droppableId
        const destinationYear = destination.droppableId
        const sourceIndex = source.index
        const destinationIndex = destination.index
        console.log("course id" + courseId)
        console.log("from " + sourseYear)
        console.log("to " + destinationYear)
        console.log("from indx " + sourceIndex)
        console.log("to " + destinationIndex)

        if (sourseYear === destinationYear) {
            return
        }
        if (destinationIndex === sourceIndex && sourseYear === destinationYear) {
            return
        }
        if (sourseYear === "mydroppable 2") {
            const copyPreviousData = {...studentCourseStore.coursedata[sourceIndex]}
            const semester = semesterCourseObject(studentId(), copyPreviousData.id, copyPreviousData.name, copyPreviousData.day, copyPreviousData.time, parseInt(destinationYear), copyPreviousData.ects)
            console.log(semester)
            const destinationYearIndex = studentCourseStore.map.get(parseInt(destinationYear))
            studentCourseStore.coursedata.splice(sourceIndex, 1)
            studentCourseStore.semesterdata[destinationYearIndex].courses.splice(destinationIndex, 0, semester)
            studentCourseStore.ectCounts += copyPreviousData.ects
        } else if (destinationYear === "mydroppable 2") {
            const sourceYearIndex = studentCourseStore.map.get(parseInt(sourseYear))
            const copyPreviousData = {...studentCourseStore.semesterdata[sourceYearIndex].courses[sourceIndex]}
            console.log(copyPreviousData)
            const course = courseObject(copyPreviousData.courses_id, copyPreviousData.name, copyPreviousData.day, copyPreviousData.time, null, copyPreviousData.ects)
            studentCourseStore.semesterdata[sourceYearIndex].courses.splice(sourceIndex, 1)
            studentCourseStore.coursedata.splice(destinationIndex, 0, course)
            studentCourseStore.ectCounts -= copyPreviousData.ects

        } else {
            const sourceYearIndex = studentCourseStore.map.get(parseInt(sourseYear))
            const destinationYearIndex = studentCourseStore.map.get(parseInt(destinationYear))
            console.log(studentCourseStore.semesterdata[sourceYearIndex])
            const copyPreviousData = {...studentCourseStore.semesterdata[sourceYearIndex].courses[sourceIndex]}
            studentCourseStore.semesterdata[sourceYearIndex].courses.splice(sourceIndex, 1)
            studentCourseStore.semesterdata[destinationYearIndex].courses.splice(destinationIndex, 0, copyPreviousData)
        }
    }

    return (
        <Box>
            <Box
                sx={{
                    display: 'flex',
                    flexDirection: 'row-reverse',
                    pt: 1,
                    pr: 1,
                    mt: 1,
                    mr: 1
                }}
            >
                <Button variant="contained" onClick={() => {
                    if (studentCourseStore.ectCounts < 180 || studentCourseStore.ectCounts > 180)
                        alert("please complete ect points")
                    else {
                        studentCourseStore.updateStudiePlanLaegger().then(r => {
                            if (r != null) alert(r)
                        })
                    }
                }}
                        style={{backgroundColor: "#9A0000", color: "white"}}>GEM</Button>
            </Box>
            <Box sx={{display: "flex", flexDirection: 'row-reverse', pr: 1, mr: 1}}>
                <p>{studentCourseStore.ectCounts}/180 ECT</p>
            </Box>
            <Container>
                <Box sx={{display: "flex", justifyContent: "center"}}>
                    <Grid container spacing={10} style={{display: "flex"}}>
                        {/*column of all semester and available courses */}
                        {/*a list of semester components*/}
                        <DragDropContext onDragEnd={handleDragEnd}>
                            <Grid item xs={12} md={6}>
                                {studentCourseStore.semesterdata.map((courses, index) =>
                                    <Grid container spacing={2} sx={{paddingBlockStart: 5}}>
                                        <Grid item xs={12}>
                                            {/*{render(courses, index)}*/}
                                            <SemesterCoursesList courses={courses} index={index} />
                                        </Grid>
                                    </Grid>
                                )}
                            </Grid>
                            {/*a list of semester available courses*/}
                            <Grid item xs={12} md={6}>
                                {/*{KurserList(studentCourseStore.coursedata)}*/}
                                <KurserList coursedata={studentCourseStore.coursedata}/>
                            </Grid>
                        </DragDropContext>
                    </Grid>
                </Box>
            </Container>
        </Box>
    )
}

export default observer(StudiePlanLaegger);
