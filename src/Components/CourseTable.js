import * as React from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';

export function createData(description, homework, assignment, files) {
    return{description,homework,assignment,files}
}


export function BasicTable(rows) {
    return (
        <TableContainer component={Paper}>
            <Table  sx={{ minWidth: 650}} aria-label="simple table" style={{borderBottom: "0px solid black"}}>
                <TableHead>
                    <TableRow style={{borderBottom:"1px solid black"}}>
                        <TableCell align="center"><h3>Week</h3></TableCell>
                        <TableCell align="center"><h3>Description</h3></TableCell>
                        <TableCell align="center"><h3>Homework</h3></TableCell>
                        <TableCell align="center"><h3>Assignments</h3></TableCell>
                        <TableCell align="center"><h3>Files</h3></TableCell>
                    </TableRow>

                </TableHead>
                <TableBody>
                    {rows.map((row, index) => (
                        <TableRow style ={ index % 2? { background : "lightgray" }:{ background : "white" }}>
                            <TableCell align={"center"} style={{borderRight: '1px solid black',borderBottom: "none",marginBottom: '0px'}}><h5>{index+1}</h5></TableCell>
                            <TableCell style={{borderRight: '1px solid black',borderBottom: "none"}}>{row.description}</TableCell>
                            <TableCell style={{borderRight: '1px solid black',borderBottom: "none"}}>{row.homework}</TableCell>
                            <TableCell style={{borderRight: '1px solid black',borderBottom: "none"}}>{<a href={row.assignment}>{row.assignment}</a>}</TableCell>
                            <TableCell style={{borderBottom: "none"}}>{<a href={row.files}>{row.files}</a>}</TableCell>
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        </TableContainer>
    );
}