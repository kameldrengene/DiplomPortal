import {Form} from "react-bootstrap";
import {Box} from "@mui/material";
import React, {useState} from "react";
import {observer} from "mobx-react";

function Semester(props) {
    const [year, setYear] = useState(new Date().getFullYear().toString());
    let semester;
    props.store.arrangeSemester(year)
    const handleChange = (event) => {
        semester = event.target.value;
        setYear(semester);
        props.store.arrangeSemester(semester);
    };

    return(
        <Box
            sx={{
                display: 'flex',
                flexDirection: 'row-reverse',
                p: 1,
                m: 1
            }}
        >
            <Form.Select id="semesteryears" style={{width: "150px", backgroundColor: "lightgray"}}
                         aria-label="Default select example" value={year} onChange={handleChange}>
                <option>Valg Semester</option>
                {props.store.semesters.map((semester) =>
                    <option key={semester.year} value={semester.year}>{semester.name}</option>
                )}

            </Form.Select>
        </Box>
    )
}
export default observer(Semester)